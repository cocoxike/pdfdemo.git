package com.xike.pdfdemo;

import com.xike.pdfdemo.dto.Demo;
import com.xike.pdfdemo.dto.Message;
import com.xike.pdfdemo.entity.DictData;
import com.xike.pdfdemo.util.HtmlToPDFUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class PdfdemoApplicationTests {


    @Resource
    private HtmlToPDFUtils htmlToPDFUtils;

    @Test
    void contextLoads() {
        Demo demo = new Demo();
        demo.setSay("这是一次测试");
        System.out.println(demo.toString());
    }


    @Test
    void testDemo(){

        htmlToPDFUtils.test();

    }


    @Test
    public void test() throws Exception{

        Message requestMessage = new Message();
        requestMessage.setReason("这是申请理由");
        requestMessage.setName("张三");
        requestMessage.setSchool("xxxxx大学");
        requestMessage.setEmail("zhangsan@abc.com");
        requestMessage.setPhone("12312345678");
        requestMessage.setSpeciality("xxx专业");
        requestMessage.setVolunteerType("类型1");
        requestMessage.setDate("xxxx年xx月xx日");

        FileSystemResource fileSystemResource = new FileSystemResource("C:\\Temp\\output.pdf");
        List<String> pickVolunteers = Stream.of("项目1", "项目7", "项目17", "项目13"
        ).collect(Collectors.toList());
        Map<String, List<DictData>> allVolunteers = new HashMap<>();
        List<DictData> adAccount = new ArrayList<>();
        DictData dict1 = new DictData();
        dict1.setDictName("项目1");
        dict1.setDictValue("项目一");
        adAccount.add(dict1);
        DictData dict2 = new DictData();
        dict2.setDictName("项目2");
        dict2.setDictValue("项目二");
        DictData dict3 = new DictData();
        dict3.setDictName("项目7");
        dict3.setDictValue("项目三");
        DictData dict4 = new DictData();
        dict4.setDictName("项目4");
        dict4.setDictValue("项目四");
        DictData dict5 = new DictData();
        dict5.setDictName("项目17");
        dict5.setDictValue("项目十七");
        DictData dict6 = new DictData();
        dict6.setDictName("项目22");
        dict6.setDictValue("项目二十二");
        DictData dict7 = new DictData();
        dict7.setDictName("项目32");
        dict7.setDictValue("项目三十二");
        DictData dict8 = new DictData();
        dict8.setDictName("项目42");
        dict8.setDictValue("项目四十二");
        DictData dict9 = new DictData();
        dict9.setDictName("项目13");
        dict9.setDictValue("项目十三");
        List<DictData> application = new ArrayList<>();
        application.add(dict2);
        application.add(dict3);
        application.add(dict4);
        application.add(dict5);
        application.add(dict6);
        application.add(dict7);
        application.add(dict8);
        application.add(dict9);
        allVolunteers.put("组织一",application);
        allVolunteers.put("组织五",adAccount);
        allVolunteers.put("组织三",application);
        allVolunteers.put("组织二",adAccount);
        allVolunteers.put("组织四",adAccount);

        List<DictData> dictData = new ArrayList<>();
        DictData dict21 = new DictData();
        dict21.setDictName("类型1");
        dict21.setRemark("类型一");
        DictData dict22 = new DictData();
        dict22.setDictName("类型2");
        dict22.setRemark("类型二");
        DictData dict23 = new DictData();
        dict23.setDictName("类型3");
        dict23.setRemark("类型三");
        DictData dict24 = new DictData();
        dict24.setDictName("类型4");
        dict24.setRemark("类型四");
        dictData.add(dict21);
        dictData.add(dict22);
        dictData.add(dict23);
        dictData.add(dict23);
        dictData.add(dict24);

        // itext方式生成
        // PdfUtils.generatePdf(fileSystemResource.getOutputStream(), requestMessage, pickVolunteers, allVolunteers, dictData);

        // 测试文本域+内容流PDFBox配置填充
        // PdfBoxUtils.generatePdf(fileSystemResource.getOutputStream(), requestMessage, pickVolunteers, allVolunteers, dictData);

        // thymeleaf模板 xhtmlrenderer html转pdf测试
        htmlToPDFUtils.htmlToPDF(fileSystemResource.getOutputStream(), requestMessage, pickVolunteers, allVolunteers, dictData);

    }
}
