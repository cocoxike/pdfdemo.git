package com.xike.pdfdemo.dto;

/**
 * Demo
 *
 * @author xike
 * @date 2023/6/5
 */
public class Demo {

    private String say;

    public String getSay() {
        return say;
    }

    public void setSay(String say) {
        this.say = say;
    }

    @Override
    public String toString() {
        return "Demo{" +
                "say='" + say + '\'' +
                '}';
    }
}
