package com.xike.pdfdemo.dto;


/**
 * Message
 *
 * @author xike
 * @date 2023/5/31
 */
public class Message {

    private String name;
    private String date;
    private String phone;
    private String email;
    private String school;
    private String speciality;
    private String reason;
    private String yesConfirm;
    private String volunteerType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getYesConfirm() {
        return yesConfirm;
    }

    public void setYesConfirm(String yesConfirm) {
        this.yesConfirm = yesConfirm;
    }

    public String getVolunteerType() {
        return volunteerType;
    }

    public void setVolunteerType(String volunteerType) {
        this.volunteerType = volunteerType;
    }
}
