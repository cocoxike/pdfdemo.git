package com.xike.pdfdemo.entity;


/**
 * 字典键值数据
 *
 * @author xike
 */
public class DictData {
    /**
     * 字典编码
     */
    private Long dictCode;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 字典类型键值
     */
    private String dictTypeValue;

    /**
     * 字典数据标签
     */
    private String dictName;

    /**
     * 字典数据键值
     */
    private String dictValue;

    private String remark;

    /**
     * 是否选中
     */
    private Boolean check;

    public Long getDictCode() {
        return dictCode;
    }

    public void setDictCode(Long dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getDictTypeValue() {
        return dictTypeValue;
    }

    public void setDictTypeValue(String dictTypeValue) {
        this.dictTypeValue = dictTypeValue;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }
}
