package com.xike.pdfdemo.config;

/**
 * PdfBoxConfig
 *
 * @author xike
 * @date 2023/5/31
 */
public class PdfBoxConfig {
    /**
     * 符号与文本之间的距离
     */
    public final static int SYMBOL_TEXT = 10;
    /**
     * 文本与下一个文本之间的距离
     */
    public final static int TEXT_TEXT = 110;
    /**
     * 文本与符号之间的距离
     */
    public final static int TEXT_SYMBOL = 32;
    /**
     * 行距
     */
    public final static int LINE_SPACING = 19;
    /**
     * 文本起始位置X
     */
    public final static int START_TEXT_X_220 = 220;
    public final static int START_TEXT_X_213 = 213;
    /**
     * 文本起始位置X
     */
    public final static int START_TEXT_X_280 = 280;
    /**
     * 文本起始位置X
     */
    public final static int START_TEXT_X_370 = 370;
    /**
     * 文本起始位置Y
     */
    public final static int START_TEXT_X_366 = 366;
    /**
     * 文本起始位置Y
     */
    public final static int START_TEXT_Y_405 = 405;
    /**
     * 文本起始位置Y
     */
    public final static int START_TEXT_Y_600 = 600;
    /**
     * 文本起始位置Y
     */
    public final static int START_TEXT_Y_720 = 720;
    /**
     * 行长
     */
    public final static int LINE_LONG = 25;
    /**
     * 复选框选中
     */
    public final static String CHECK_BOX = "☑";
    /**
     * 复选框未选中
     */
    public final static String UN_CHECK_BOX = "☐";
}
