package com.xike.pdfdemo.util;

import com.lowagie.text.pdf.BaseFont;
import com.xike.pdfdemo.dto.Message;
import com.xike.pdfdemo.entity.DictData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * HtmlToPDFUtils
 *
 * @author xike
 * @date 2023/3/30
 */
@Component
public class HtmlToPDFUtils {

    @Autowired
    private TemplateEngine templateEngine;

    final static String FONT_SIMHEI = "src/main/resources/fonts/simhei.ttf";
    final static String TEMPLATE = "applying_for_volunteer_service_form_html";

    /**
     * html转PDF
     * @param outputStream 文件输出流
     * @param message 信息
     * @param pickVolunteers 选中的志愿项
     * @param allVolunteers 所有志愿项目
     * @param volunteerTypes 志愿类型
     * @throws IOException
     */
    public void htmlToPDF(OutputStream outputStream, Message message, List<String> pickVolunteers, Map<String, List<DictData>> allVolunteers, List<DictData> volunteerTypes) throws IOException {

        Context context = new Context();
        context.setVariable("requestMessage", message);
        buildApplicationType(message.getVolunteerType(),volunteerTypes);
        context.setVariable("volunteerTypes",  volunteerTypes);
        buildAllAvailableApp(pickVolunteers,allVolunteers);
        context.setVariable("allVolunteers",  allVolunteers);
        context.setVariable("yesConfirm",  true);
        context.setVariable("noConfirm",  false);

        // 模板数据转换
        String htmlStr = templateEngine.process(TEMPLATE, context);

        ITextRenderer renderer = new ITextRenderer();
        SharedContext sharedContext = renderer.getSharedContext();
        // 打印
        sharedContext.setPrint(true);
        // 互动
        sharedContext.setInteractive(false);
        // 设置中文字体
        ITextFontResolver fontResolver = renderer.getFontResolver();
        fontResolver.addFont(FONT_SIMHEI, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

        renderer.setDocumentFromString(htmlStr);
        renderer.layout();
        renderer.createPDF(outputStream);
        renderer.finishPDF();
    }

    /**
     * 构建权限选中项
     * @param pickVolunteers 已选中的
     * @param allVolunteers 所有
     */
    private void buildAllAvailableApp(List<String> pickVolunteers, Map<String, List<DictData>> allVolunteers) {
        if(CollectionUtils.isEmpty(pickVolunteers)){
            return;
        }
        if(CollectionUtils.isEmpty(allVolunteers)){
            return;
        }
        for (Map.Entry<String, List<DictData>> entry : allVolunteers.entrySet()) {
            List<DictData> value = entry.getValue();
            if(CollectionUtils.isEmpty(value)){
                continue;
            }
            for(DictData dictDataResult : value){
                if(pickVolunteers.contains(dictDataResult.getDictName())){
                    dictDataResult.setCheck(true);
                } else {
                    dictDataResult.setCheck(false);
                }
            }
        }
    }

    /**
     * 构建类型选中项
     * @param volunteerType
     * @param volunteerTypes
     */
    private void buildApplicationType(String volunteerType, List<DictData> volunteerTypes) {
        for (DictData dictData : volunteerTypes){
            if(volunteerType.equals(dictData.getDictName())){
                dictData.setCheck(true);
            } else {
                dictData.setCheck(false);
            }
        }
    }

    public void test() {
        System.out.println("这是一次实验");
    }
}
